p0 = [0 0];
pe = [4 11];
L1 = 5;
L2 = 6;
L3 = 2;
%c1 k�r:
hold on;
axis equal;%x-y egym�shoz igaz�t�s
%magic:
th = 0:pi/50:2*pi;
xunit = L1 * cos(th) + p0(1);
yunit = L1 * sin(th) + p0(2);
%kirajzol�s:
h = plot(xunit, yunit);
%c3 k�r:
%magic:
th = 0:pi/50:2*pi;
xunit = L3 * cos(th) + pe(1);
yunit = L3 * sin(th) + pe(2);
%kirajzol�s:
h = plot(xunit, yunit);
%k�z�ppontok kirajzol�sa:
plot(p0(1), p0(2), 'bo');
plot(pe(1), pe(2), 'ro');

%L2 + L3 k�r:
th = 0:pi/50:2*pi;
xunit = (L2 + L3) * cos(th) + pe(1);
yunit = (L2 + L3) * sin(th) + pe(2);
%kirajzol�s:
h = plot(xunit, yunit);

%L2 - L3 k�r:
th = 0:pi/50:2*pi;
xunit = (L2 - L3) * cos(th) + pe(1);
yunit = (L2 - L3) * sin(th) + pe(2);
%kirajzol�s:
h = plot(xunit, yunit);

%csukl� anat. szab�ly:
Ls = sqrt(L2 * L2 + L3 * L3);
th = 0:pi/50:2*pi;
xunit = Ls * cos(th) + pe(1);
yunit = Ls * sin(th) + pe(2);
%kirajzol�s:
h = plot(xunit, yunit);

%k�ny�k anat. szab�ly:
x = sin(35 * pi / 180) * L1;%rad-deg v�lt�s
L2_ = sqrt(L1 * L1 - x * x);
L2__ = L2 - L2_;
r = sqrt(L2__ * L2__ + x * x);
th = 0:pi/50:2*pi;
xunit = r * cos(th) + p0(1);
yunit = r * sin(th) + p0(2);
%kirajzol�s:
h = plot(xunit, yunit);